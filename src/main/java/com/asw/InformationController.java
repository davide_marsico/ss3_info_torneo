package com.asw;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* This controller gives informations about tournaments
*
* @author  Alessio Buscemi 
*/

@Configuration
@PropertySource("application.yml")
@RestController
public class InformationController {


	@Value("${grounds}")
	private String grounds;



	@RequestMapping("/{giorno}")
	public String getGrounds(@PathVariable String giorno) {


		String[] wordArray = grounds.split(",");
		int i1 = (int) (Math.round(Math.random()*(wordArray.length-1)));
		int i2 = (int) (Math.round(Math.random()*(wordArray.length-1)));
		int i3 = (int) (Math.round(Math.random()*(wordArray.length-1)));
		int i4 = (int) (Math.round(Math.random()*(wordArray.length-1)));


		// per non estrarre campi uguali
		while(i2 == i1)
			i2 = (int) (Math.round(Math.random()*(wordArray.length-1)));

		// per non estrarre campi uguali
		while(i3 == i2 | i3 == i1)
			i3 = (int) (Math.round(Math.random()*(wordArray.length-1)));

		while(i4 == i1 | i4 == i2 | i4 == i3)
			i4 = (int) (Math.round(Math.random()*(wordArray.length-1)));

		String res = wordArray[i1] + "," +  wordArray[i2] + "," + wordArray[i3] + "," + wordArray[i4];

		return res;

	}




	@RequestMapping("/{giorno}/{costo}")
	public String getTournamentFee(@PathVariable String giorno,@PathVariable String costo) {

		String[] courtArray = grounds.split(",");
		int i1 = (int) (Math.round(Math.random()*(courtArray.length-1)));
		int i2 = (int) (Math.round(Math.random()*(courtArray.length-1)));
		int i3 = (int) (Math.round(Math.random()*(courtArray.length-1)));
		int i4 = (int) (Math.round(Math.random()*(courtArray.length-1)));


		// per non estrarre campi uguali
		while(i2 == i1)
			i2 = (int) (Math.round(Math.random()*(courtArray.length-1)));

		// per non estrarre campi uguali
		while(i3 == i2 | i3 == i1)
			i3 = (int) (Math.round(Math.random()*(courtArray.length-1)));
		
		// per non estrarre campi uguali
		while(i4 == i1 | i4 == i2 | i4 == i3)
			i4 = (int) (Math.round(Math.random()*(courtArray.length-1)));

		int max = Integer.parseInt(costo);
		
	    //il prezzo è fino a 20 euro
		int c1 = (int) (Math.round(Math.random()*(max)));
		int c2 = (int) (Math.round(Math.random()*(max)));
		int c3 = (int) (Math.round(Math.random()*(max)));
		int c4 = (int) (Math.round(Math.random()*(max)));



		String res = courtArray[i1] + " - prezzo: "+ c1 +" euro, "+  courtArray[i2] + " - prezzo: "+ c2 +" euro, " + courtArray[i3] + " - prezzo: "+ c3 +" euro, " + courtArray[i4] +" - prezzo: "+ c4 +" euro.";

		return res;


	}



}
