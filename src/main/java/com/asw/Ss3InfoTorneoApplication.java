package com.asw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss3InfoTorneoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss3InfoTorneoApplication.class, args);
	}
}
